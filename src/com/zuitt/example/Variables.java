package com.zuitt.example;

//naming convention
/* for variables, use camelCase */

// Variable declaration:



public class Variables {
    public static void main (String[] args){
        int age;
        char middleName;
        int x;
        int y = 0 ;
        x = 1;
        middleName = 'I';

        System.out.println("The value of y is " + y + " and the value of x is " + x +".");
        float floatNumber = 3.1232312312312f;
        double doubleNumber = 3.1232312312312;
        long longNumber = 101123123123123121L;
        int intNumber = 1011231231;
        boolean isNice = true;
//        when using long, put L at the last to indicate data type
//        when using float, put f at the last to indicate data type
        System.out.println("Int: " + intNumber);
        System.out.println("Long: " + longNumber);
        System.out.println("Char: " +  middleName);
        System.out.println("Float: " +  floatNumber);
        System.out.println("Double: " +  doubleNumber);
        System.out.println("Boolean: " +  !isNice);

//        constants
//        "final" keyword indicates that the variable's value cannot be changed. example:

        final int PRINCIPAL = 3000;
        System.out.println("Constant: " + PRINCIPAL);

        String username = "JSmith";
        System.out.println("String: " + username);

        int stringLength = username.length();
        System.out.println("Length Method: "+ stringLength);
    }
}
